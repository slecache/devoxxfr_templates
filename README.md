# README #

Devoxx France 2017 template

### What is this repository for? ###

This repository contains different templates for Devoxx France 2017. It's not a closed repository, you can also contribute and propose your presentation.

As an inspiration, we included a PDF version of our Keynote template, so that you can get an idea of what it could looks like.
See https://bitbucket.org/nicolas_martignole/devoxxfr_templates/raw/191b018584f923eb7f87e31db2d8dc0a17659af9/Sample%20presentation%20Devoxx%20FR%202017.pdf

### How do I contribute ###

If you want to create your own Devoxx France 2017, we'd like that you use our guidelines, fonts and logo so that we can offer a consolidated experience to our attendees.

### Devoxx France 2017 Fonts and Colors

### Fonts

#### Titles

For main title, we used Montserrat (https://www.google.com/fonts/specimen/Montserrat) 
You can use Montserrat semi-bold for titles.
See the Devoxx France web site as a live version. 

#### Main text

For main text we use Open Sans, one of the Google Font, with variant Normal, 400

#### Direct Google font link

You can use this

<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans' rel='stylesheet' type='text/css'>

#### Color chart 1

The main Theme is based on the following colors :

main color, light blue, #057FFA
secondary color, dark blue, #03002E
third color, alternate blue #033494

alternate color, accent, #F2D279
alternate color 2, #EDAF1F (orange from Devoxx logo)

### What is the Devoxx France 2017 Theme?  

A tree with the Artificial energy

### What is the recommended format? 

Almost all presentations are recorded. Your presentation will be available few weeks after Devoxx France 2017 on the Youtube Devoxx channel. 

We recommend that you keep the 16/9 ratio format for optimal display.

This year we record your laptop video output directly and thus we won't need PDF or your slides after the presentation. Feel free to use any presentation technologies, but try to follow our guidelines and the color chart.

Devoxx France are not mandatory. If you prefer to use your own presentation, do it. Please insert a first slide with the Devoxx France 2017 theme, then keep your presentation as it is. Always remember : your presentation is recorded. 

Thanks

Nicolas Martignole
nicolas.martignole@devoxx.fr


